<?php
if ($_SERVER["REQUEST_METHOD"] !== "POST") return;
require __DIR__ . '/vendor/autoload.php';

use app\App;
use app\Request;
use YandexCheckout\Client;

$request = new Request($_POST);

$client = new Client();
$client->setAuth('some_id', 'some_key');

$app = new App($request, $client);

$response = $app->createPayment('some_domain');
echo $response->getConfirmation()->getConfirmationUrl();