<?php
require __DIR__ . '/vendor/autoload.php';

use app\Callback;

$callback = new Callback();

if ($callback->isJSONRequest() && $callback->isSuccessPayment()) {
    $callback->sendMail('morfilius88@gmail.com');
}