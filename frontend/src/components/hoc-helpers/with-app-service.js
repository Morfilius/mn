import React from 'react';
import {AppServiceConsumer} from "../app-service-context/app-service-context";

const WithAppService = (mapMethodsToProps) => (Wrapped) => {
    return (props) => {

        return (
            <AppServiceConsumer>
                {
                    (appService) => {
                        const serviceProps = mapMethodsToProps(appService);

                        return <Wrapped {...props} {...serviceProps}/>
                    }


                }
            </AppServiceConsumer>
        )
    }
};

export default WithAppService;