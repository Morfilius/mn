import React from "react";

const Card = ({id, imagePath, subtitle, title, offset, price, onAddToCart, scrollTop}) => {
    return (
        <div className="card">
            <img
                className="section-additionally-block-item-block-img card__img"
                alt=""
                src={imagePath}
            />
            <span className="card__subtitle">{subtitle}</span><br/>
            <span
                className="section-additionally-block-item-block-hedline card__title">{title}</span>
            <a href="#" onClick={(e) => {
                e.preventDefault();
                scrollTop(id, offset);
            }} data-offset="20" className="show-more"></a>
            <div className="section-additionally-block-item-block-button clickAddCart add-to-cart">
                <span className="card__price">{price} руб</span>
                <div className="section-additionally-block-item-block-button-text add-to-cart__btn" onClick={() => onAddToCart(id)}>Добавить в корзину</div>
            </div>
        </div>
    );
};

export default Card