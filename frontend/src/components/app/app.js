import React, {useState} from "react";
import {Route, BrowserRouter as Router, Switch, Redirect} from "react-router-dom";
import HomePage from "../pages/home-page";
import Header from "../header/header";
import Footer from "../footer/footer";
import {AppServiceProvider} from "../app-service-context/app-service-context";

import './sass/libs.sass'
import './sass/style.sass'
import './sass/slick.scss'
import CartPage from "../pages/cart-page";
import AppService from "../../services/AppService";
import SuccessPage from "../pages/success-page";

import * as Scroll from 'react-scroll';
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Change from "../modal/change";
import Delivery from "../modal/delivery";
import Pay from "../modal/pay";
import Contact from "../modal/contact";
import Policy from "../modal/policy";
import GoogleTagService from "../../services/GoogleTagService";

const App = () => {

    const promoWord = 'GREEN';
    const promoPercent = 10;

    const [ products, setProduct ] = useState([]);
    const [ cart, setCart] = useState([]);
    const [ appService, setAppService] = useState(new AppService())
    const [ totalCost, setTotalCost] = useState(0)
    const [ deliveryCost, setDeliveryCost] = useState(0)
    const [ formFields, setFormFields] = useState(0)
    const [ orderNumber, setOrderNumber] = useState(0)
    const [ isUsePromo, setUsePromo] = useState(false)

    const [changeModalShow, setChangeModalShow] = React.useState(false);
    const [deliveryModalShow, setDeliveryModalShow] = React.useState(false);
    const [payModalShow, setPayModalShow] = React.useState(false);
    const [contactsModalShow, setContactsModalShow] = React.useState(false);
    const [policyModalShow, setPolicyModalShow] = React.useState(false);

    const tagService = new GoogleTagService();

    const scrollTop = (id, offset) => {
        const el = document.querySelector(`#product-${id}`)

        scroll.scrollTo(+el.offsetTop + offset);
    }

    const togglePromo = (word) => {
        setUsePromo(state => word === promoWord)
    }

    const onAddToCart = (id) => {
        const product = getProduct(id);
        if (product) {
            tagService.addToCart(product);
            setCart((state) => {
                const updatedState = increaseCount(state, id, 'count');
                if (updatedState) {
                    return updatedState;
                }
                return  [...state, {id: product.id, product, count: 1}]
            });
        }
    }

    const getProduct = (id) => {
        return products.find((item) => {
            return item.id === id;
        })
    }

    const deleteItemCart = (id) => {
        const product = getProduct(id);
        tagService.removeFromCart(product);
        setCart((state) => {
            return [...state.filter((item) => item.id !== id)];
        })
    }

    const increaseCountCartItem = (id) => {
        const product = getProduct(id);
        tagService.addToCart(product);
        setCart((state) => {
            return increaseCount(state, id);
        });
    }

    const decreaseCountCartItem = (id) => {
        const product = getProduct(id);
        tagService.removeFromCart(product);
        setCart((state) => {
            return decreaseCount(state, id);
        });
    }

    const increaseCount = (state, id) => {
        return _changeProperty(state, id, (oldItem) => {
            return {...oldItem, count: ++oldItem.count}
        })
    }

    const decreaseCount = (state, id) => {
        return _changeProperty(state, id, (oldItem) => {
            if (oldItem.count <= 1) {
                return {...oldItem};
            }
            return {...oldItem, count: --oldItem.count}
        })
    }

    const assignTotalCost = () => {
        let productsCost = cart.reduce((previousValue, currentValue) => {
                                    previousValue += (currentValue.product.price * currentValue.count);
                                    return previousValue;
                                }, 0);

        if (isUsePromo) {
            productsCost = productsCost - (productsCost/100 * promoPercent)
        }

        const totalCost = productsCost + deliveryCost;

        setTotalCost(totalCost)
    }

    const assignDeliveryCost = (cost) => {
        if (deliveryCost !== +cost) {
            setDeliveryCost(+cost)
        }
    }

    const assignFormFields = (fields) => {
        setFormFields(fields)
    }

    const assignOrderNumber = (number) => {
        setOrderNumber(+number)
    }

    const _changeProperty = (arr, id, newItemFunc) => {
        if (!arr.length) return false;

        const idx = arr.findIndex((el) => el.id === id);

        if (idx < 0) return false;

        const oldItem = arr[idx];
        const newItem = newItemFunc(oldItem);

        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];
    }

    const isCartEmpty = () => {
        return ! cart.length;
    }

    return (
        <AppServiceProvider value={appService}>
            <div>
                <Router>
                    <Switch>
                        <Route path="/" exact>
                            <Header cart={cart} scrollTop={scrollTop} setContactsModalShow={setContactsModalShow}
                                    setDeliveryModalShow={setDeliveryModalShow} setPolicyModalShow={setPolicyModalShow}
                                    setChangeModalShow={setChangeModalShow}/>
                            <HomePage products={products} onAddToCart={onAddToCart} setProduct={setProduct} scrollTop={scrollTop}/>
                            {/*modals*/}
                            <Change show={changeModalShow} onHide={() => setChangeModalShow(false)}/>
                            <Delivery show={deliveryModalShow} onHide={() => setDeliveryModalShow(false)}/>
                            <Pay show={payModalShow} onHide={() => setPayModalShow(false)}/>
                            <Contact show={contactsModalShow} onHide={() => setContactsModalShow(false)}/>
                            <Policy show={policyModalShow} onHide={() => setPolicyModalShow(false)}/>

                            <Footer
                                products={products} scrollTop={scrollTop}
                                setChangeModalShow={setChangeModalShow}
                                setDeliveryModalShow={setDeliveryModalShow}
                                setPayModalShow={setPayModalShow}
                            />
                        </Route>
                        <Route path="/cart" exact>
                            {
                                (! isCartEmpty())
                                    ?
                                    <CartPage
                                        cart={cart}
                                        togglePromo={togglePromo}
                                        increaseCountCartItem={increaseCountCartItem}
                                        decreaseCountCartItem={decreaseCountCartItem}
                                        totalCost={totalCost}
                                        assignTotalCost={assignTotalCost}
                                        deleteItemCart={deleteItemCart}
                                        assignDeliveryCost={assignDeliveryCost}
                                        assignFormFields={assignFormFields}
                                        assignOrderNumber={assignOrderNumber}
                                        orderNumber={orderNumber}
                                    />
                                    : <Redirect to="/" />
                            }

                        </Route>
                        <Route path="/success" exact>
                            {
                                (! isCartEmpty())
                                    ?
                                    <SuccessPage
                                        cart={cart}
                                        orderNumber={orderNumber}
                                        totalCost={totalCost}
                                        deliveryCost={deliveryCost}
                                        formFields={formFields}
                                        isUsePromo={isUsePromo}
                                    />
                                    : <Redirect to="/" />
                            }
                        </Route>
                    </Switch>
                </Router>
            </div>
        </AppServiceProvider>
    );
};

export default App;