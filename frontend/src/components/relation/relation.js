import React from "react";
import Slider from "react-slick";
import Item from "./item";

const Relation = ({products, onAddToCart}) => {
    const settings = {
        dots: false,
        nextArrow: <NextArrow/>,
        prevArrow: <PrevArrow/>,
        slidesToShow: 3
    };
    return (
        <div className="relation section section--leaf pt200">
            <div className="section-inner">
                <div className="title-center">
                    <span className="relation__title section-title">Вам могут быть интересны</span><br/>
                    <div className="section-line"></div>
                </div>
                <div className="relation-slider">
                    <Slider {...settings}>
                        {
                            products.filter((item) => item.type === 'item').map((item) => <Item onAddToCart={onAddToCart} key={item.id} {...item}/>)
                        }
                    </Slider>
                </div>
            </div>
        </div>
    );
};

const NextArrow = ({ className, style, onClick }) => {
    return (
        <span
            className={`next ${className}`}
            style={{ ...style}}
            onClick={onClick}
        />
    );
};


const PrevArrow = ({ className, style, onClick }) => {
    return (
        <span
            className={`prev ${className}`}
            style={{ ...style}}
            onClick={onClick}
        />
    );
};

export default Relation