import React from 'react';

const Item = ({id, title, imagePath, text, price, onAddToCart}) => (
    <div className="relation-slider__item">
        <div className="card">
            <img className="section-additionally-block-item-block-img card__img" alt=""
                 src={imagePath}/>
            <span className="section-additionally-block-item-block-hedline card__title">{title}</span>
            <p className="card__text">
                {text}
            </p>
            <span className="card__price relation-slider__price">{price} руб</span>
            <div
                className="section-additionally-block-item-block-button clickAddCart add-to-cart" onClick={() => onAddToCart(id)}>
                <div
                    className="section-additionally-block-item-block-button-text add-to-cart-with-arrow"></div>
            </div>
        </div>
    </div>
);

export default Item;