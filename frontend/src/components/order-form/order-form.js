// Render Prop
import React, {useEffect, useState} from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(3, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    lastName: Yup.string()
        .min(3, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    phone: Yup.string()
        .min(7, 'Минимум 7 символов')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    locality: Yup.string()
        .min(3, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    street: Yup.string()
        .min(3, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    house: Yup.string()
        .min(1, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    flat: Yup.string()
        .min(1, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
        .required('Обязательное поле'),
    email: Yup.string()
        .email('Неправильный email')
        .required('Обязательное поле'),
    code: Yup.string()
        .min(3, 'Слишком короткиая строка')
        .max(50, 'Слишком длинная строка')
});

const OrderForm = ({assignDeliveryCost, totalCost, assignFormFields, assignOrderNumber, togglePromo}) => (

    <Formik
        initialValues={{
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            locality: '',
            street: '',
            house: '',
            flat: '',
            comment: '',
            delivery: 300,
            method: 1,
            code: ''
        }}
        validationSchema={SignupSchema}
        onSubmit={ values => {
            assignFormFields(values);
            let formData = new FormData();
            formData.append("phone", values.phone);
            let response = fetch('/saveorder.php', {
                method: 'POST',
                body: formData
            });
            response
                .then((response) => response.json())
                .then((data) => {
                    assignOrderNumber(+data)
                })
        }}
        children={props => <FormContent {...props} togglePromo={togglePromo} assignDeliveryCost={assignDeliveryCost} totalCost={totalCost}/>}
    />
);

const FormContent = ({errors, touched, handleChange, handleBlur, values, assignDeliveryCost, totalCost, togglePromo}) => {

    const freeDeliveryTotalPrice = 3000;
    const [isFreeDelivery, setFreeDelivery] = useState(false)


    useEffect(() => {
        if(isFreeDelivery) {
            assignDeliveryCost(0);
        }else {
            assignDeliveryCost(+values.delivery);
        }
        togglePromo(values.code);
    });

    if(totalCost >= freeDeliveryTotalPrice && !isFreeDelivery) {
        setFreeDelivery(true);
    } else if(totalCost < freeDeliveryTotalPrice && isFreeDelivery) {
        setFreeDelivery(false);
    }

    return (
        <Form>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Имя</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.firstName}
                           name="firstName"
                    />
                    {errors.firstName && touched.firstName ? (
                        <div className='error'>{errors.firstName}</div>
                    ) : null}
                </div>
            </div>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Фамилия</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input" type="text"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.lastName}
                           name="lastName"
                    />
                    {errors.lastName && touched.lastName ? (
                        <div className='error'>{errors.lastName}</div>
                    ) : null}
                </div>

            </div>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Номер телефона</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input" type="text"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.phone}
                           name="phone"
                    />
                    {errors.phone && touched.phone ? (
                        <div className='error'>{errors.phone}</div>
                    ) : null}
                </div>

            </div>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Email</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.email}
                           name="email"
                    />
                    {errors.email && touched.email ? (
                        <div className='error'>{errors.email}</div>
                    ) : null}
                </div>

            </div>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Способ доставки</div>

                    {
                        isFreeDelivery
                        ? (
                            <div className="delivery">
                                <span>Бесплатная доставка от {freeDeliveryTotalPrice} руб.</span>
                            </div>
                            )
                        : (
                            <div className="delivery">
                                <input type="radio" className="mail" id="delivery1" defaultChecked
                                       onChange={handleChange}
                                       onBlur={handleBlur}
                                       value={300}
                                       name="delivery"

                                />
                                <label htmlFor="delivery1">до пунтка выдачи СДЭК <span className="deliveryPrice">300р</span></label><br/><br/>
                                <input type="radio" className="cour" id="delivery2"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={500}
                                name="delivery"/>
                                <label htmlFor="delivery2">до двери <span className="deliveryPrice">500р</span> </label><br/>
                            </div>
                        )
                    }

            </div>
            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Населённый пункт</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.locality}
                           name="locality"
                    />
                    {errors.locality && touched.locality ? (
                        <div className='error'>{errors.locality}</div>
                    ) : null}
                </div>
            </div>

            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Адрес</div>
                <div className="basket-block-of-form-left-line-address">
                    <div className="error-wrap error-wrap--address">
                        <input className="basket-block-of-form-left-line-address-input" type="text"
                               placeholder="Улица"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.street}
                               name="street"
                        />
                        {errors.street && touched.street ? (
                            <div className='error'>{errors.street}</div>
                        ) : null}
                    </div>
                <div className="basket-block-of-form-left-line-address-block">
                    <div className="error-wrap">
                        <input className="basket-block-of-form-left-line-address-block-input" type="text"
                               placeholder="Дом"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.house}
                               name="house"
                        />
                        {errors.house && touched.house ? (
                            <div className='error'>{errors.house}</div>
                        ) : null}
                    </div>
                    <div className="error-wrap">
                        <input className="basket-block-of-form-left-line-address-block-input" type="text"
                               placeholder="Квартира"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.flat}
                               name="flat"
                        />
                        {errors.flat && touched.flat ? (
                            <div className='error'>{errors.flat}</div>
                        ) : null}
                    </div>
                </div>
            </div>
            </div>

            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Комментарий</div>
                <textarea className="basket-block-of-form-left-line-textarea"
                       onChange={handleChange}
                       onBlur={handleBlur}
                       value={values.comment}
                       name="comment"
                />
            </div>


            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Способ оплаты</div>
                <div className='model'>
                    <input className="clickPay" type="radio" id="do1"  defaultChecked
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={1}
                           name="method"


                    />
                        <label htmlFor="do1">предоплата (картой онлайн)</label>
                        <br/><br/>
                </div>
            </div>

            <div className="basket-block-of-form-left-line">
                <div className="basket-block-of-form-left-line-text">Промокод</div>
                <div className="error-wrap">
                    <input className="basket-block-of-form-left-line-input"
                           onChange={handleChange}
                           onBlur={handleBlur}
                           value={values.code}
                           name="code"
                    />
                    {errors.code && touched.code ? (
                        <div className='error'>{errors.code}</div>
                    ) : null}
                </div>
            </div>

            <div className="basket-block-of-form-left-title">Итого к оплате: <span className="showAllPrice">{totalCost}</span>
                руб.
            </div>
            <div className="basket-block-of-form-left-per clickSp">
                <button className="orderButton" value="Отправить заявку">Отправить заявку<img alt='' src="./img/arrow1.svg"/></button>
            </div>
        </Form>
    )
}


export default OrderForm;