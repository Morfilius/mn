import React, {useEffect} from 'react';
import Item from "./item";

const Cart = ({cart, increaseCountCartItem, decreaseCountCartItem, assignTotalCost, deleteItemCart}) => {
    useEffect(() => {
        assignTotalCost()
    });

    return (
        <div className="basket-block-order" style={{marginTop: '100px'}}>
            <div className="basket-block-order-title">Ваш заказ</div>
            <div className="basket-block-order-line"></div>
            {
                cart.map((item) => {
                    return <Item key={item.id} item={item} increaseCountCartItem={increaseCountCartItem}
                                 decreaseCountCartItem={decreaseCountCartItem} deleteItemCart={deleteItemCart}/>
                })
            }
        </div>
    )
};

export default Cart;