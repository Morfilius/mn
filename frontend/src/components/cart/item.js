import React from 'react';

const Item = ({item, increaseCountCartItem, decreaseCountCartItem, deleteItemCart}) => {
    return (
        <div className="basket-block-order-item">
            <div className="basket-block-order-item-block item5">
                <div className="basket-block-order-item-block-img"
                     style={{ backgroundImage: `url(${item.product.imagePath})`}}>
                </div>
                <div className="basket-block-order-item-block-text">
                    <div className="basket-block-order-item-block-text-z">Товар</div>
                    <div className="basket-block-order-item-block-text-a">{item.product.title}</div>
                </div>
                <div className="basket-block-order-item-block-text">
                    <div className="basket-block-order-item-block-text-z">Цена</div>
                    <div className="basket-block-order-item-block-text-a">{item.product.price}</div>
                </div>
                <div className="basket-block-order-item-block-col">
                    <div className="basket-block-order-item-block-col-bl" onClick={() => increaseCountCartItem(item.id)}>+</div>
                    <div className="basket-block-order-item-block-col-text showColitem5">{item.count}</div>
                    <div className="basket-block-order-item-block-col-bl" onClick={() => decreaseCountCartItem(item.id)}>-</div>
                </div>
                <div className="basket-block-order-item-block-text">
                    <div className="basket-block-order-item-block-text-z">Сумма</div>
                    <div className="basket-block-order-item-block-text-a showPriceItem5">{item.product.price * item.count} руб.</div>
                </div>
                <div className="basket-block-order-item-block-delete" onClick={() => deleteItemCart(item.id)}>
                    <img className="clickDelete" src="./img/delete.svg"/>
                </div>
            </div>
        </div>
    )
};

export default Item;