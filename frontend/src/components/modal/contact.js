import React from 'react';
import {Modal} from "react-bootstrap";


const Contact = (props) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    Контакты
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="row">
                <div className="col-3">
                    <span className="contact-title">Название компании</span><br/>
                    <span className="contact-text">ООО "НАТУРКОСМЕТИКС</span>
                </div>
                <div className="col-3">
                    <span className="contact-title">Адрес</span><br/>
                    <span className="contact-text">Площадь Журавлёва 2с2</span>
                </div>
                <div className="col-3">
                    <span className="contact-title">Телефон</span><br/>
                    <span className="contact-text"><a className="contact-link" href="tel:+84991101120">+84991101120</a></span>
                </div>
                <div className="col-3">
                    <span className="contact-title">Телефон</span><br/>
                    <span className="contact-text"><a className="contact-link" href="mailto:Info@makeandnature.ru">Info@makeandnature.ru</a></span>
                </div>
            </div>
            <div className="row">
                <div className="col-3">
                    <span className="contact-title">Время работы</span><br/>
                    <span className="contact-text">пн-пт с 10:00 - 18:00 </span>
                </div>
                <div className="col-3">
                    <span className="contact-title">Почтовый адрес</span><br/>
                    <span className="contact-text">Площадь Журавлёва 2с2</span>
                </div>
                <div className="col-3">
                    <span className="contact-title">Телефон</span><br/>
                    <span className="contact-text"><a className="contact-link" href="tel:+78006002758">+78006002758</a></span>
                </div>
                <div className="col-3">
                    <span className="contact-title">ИНН/ОГНР</span><br/>
                    <span className="contact-text">3664069397/1053600591197</span>
                </div>
            </div>
            </Modal.Body>
        </Modal>
    );
};

export default Contact;