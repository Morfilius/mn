import React from 'react';
import {Modal} from "react-bootstrap";


const Delivery = (props) => {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    Доставка транспортной компании СДЭК
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    <p><b>Доставка в пределах МКАД:</b></p>
                    <p>Стоимость доставки в пределах МКАД 350 рублей.</p>
                    <p> При сумме заказа более 3000 рублей – доставка бесплатна</p>
                    <p><b>Доставка по России:</b></p>
                    <p>- до 3000 рублей - 350 рублей</p>
                    <p>- от 3000 рублей - бесплатно</p>
                    <p><b>1. Курьер по России 3-5 рабочих дней</b></p>
                    <p><b> 2. До пункта самовывоза 2-5 рабочих дней</b></p>
                    *Срок доставки может быть увеличен курьерской службой по независящим от нас причинам
                </p>
            </Modal.Body>
        </Modal>
    );
};

export default Delivery;