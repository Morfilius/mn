import React from "react";
import {Link} from "react-router-dom";

const Header = ({cart, scrollTop, setContactsModalShow, setChangeModalShow,
                    setDeliveryModalShow, setPolicyModalShow}) => {
    const count = () => {
        return cart.reduce((prev, item) => {
            prev+= +item.count
            return prev;
        }, 0)
    }

    return (<div className="section-header section-outer">
        <div className="section-header-line-1"></div>
        <div className="section-header-line-2"></div>
        <div className="section-inner">
            <div className="section-header-content">
                <span className="header-schedule">Время работы 9:00-18:00</span>
                <img src="img/header-title.svg" alt="" className="header-title"/>
                <span className="header-phones">
                    <a href="tel:+84991101120" className="header-phone">+84991101120</a>
                    <a href="tel:+78006002758" className="header-phone">+78006002758</a>
                </span>
            </div>
            <div className="section-header-menu">
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(1, 30)
                }}>Гоммаж</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(2, 120)
                }}>Маска</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(3, 120)
                }}>Крем</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(6, 120)
                }}>Щётки для тела</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(4, 120)
                }}>Массажное мыло</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(7, 120)
                }}>Экосвеча</a>
                <a href="#" onClick={(e) => {
                    e.preventDefault();
                    scrollTop(8, -130)
                }}>Наборы</a>
                <div className="with-submenu">Информация
                    <ul className="submenu">
                        <li><a href="#" onClick={(e) => {
                            e.preventDefault()
                            setContactsModalShow(true)
                        }}>Контакты</a></li>
                        <li><a href="#" onClick={(e) => {
                            e.preventDefault()
                            setDeliveryModalShow(true)
                        }}>Доставка и оплата</a></li>
                        <li><a href="#" onClick={(e) => {
                            e.preventDefault()
                            setPolicyModalShow(true)
                        }}>Политика конфиденциальности</a></li>
                        <li><a href="#" onClick={(e) => {
                            e.preventDefault()
                            setChangeModalShow(true)
                        }}>Обмен и возврат</a></li>
                    </ul>
                </div>
                <Link className="section-header-block-shop" to="/cart">
                    <img className="section-header-block-shop-img" src="./img/icon-shop.svg"/>
                    <div className="section-header-block-shop-text showBasket">Корзина {count() ? `(${count()})` : '' }</div>
                </Link>
            </div>
        </div>
    </div>);
};

export default Header