import React, {Fragment} from "react";

const Set = ({id, title, subtitle, price, priceOld, text, thumbs, bgVariant, onAddToCart}) => {
    return (
        <Fragment>
            <div className="sect-line"></div>
            <div className={`set section section--no-p ${bgVariant === 2 ? 'set--variant2' : ''}`} id={`product-${id}`}>
                <div className="set__content">
                    <div className="section-inner">
                        <span className="set__subtitle">{subtitle}</span><br/>
                        <span className="set__title section-title">{title}</span><br/>
                        <span className="set__price-wrap">
                        <span className="set__price">{price} руб</span>
                        <span className="set__price-old">{priceOld} руб</span>
                    </span>
                        <p className="set__text">
                            {text}
                        </p>
                        <div className="section-additionally-block-item-block-button clickAddCart add-to-cart" onClick={() => onAddToCart(id)}>
                            <div
                                className="section-additionally-block-item-block-button-text add-to-cart-with-arrow"></div>
                        </div>
                    </div>

                </div>
                <Thumbs thumbs={thumbs}/>
            </div>
        </Fragment>
    );
};

const Thumbs = ({thumbs}) => {
    return (
        <div className="set__images">
            {
                thumbs.map((thumb) => {
                    return (
                        <div key={thumb} className="set__images-wrap">
                            <img src={thumb} alt=""/>
                        </div>
                    )
                })
            }
        </div>
    );
}

export default Set