import React, {Fragment} from "react";
import Slider from "react-slick";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

const Item = ({id, title, detailImage, price, volume, thumbs, reverse, onAddToCart, effect, composition, mode}) => {
    const rightSide = <RightSide title={title} price={price} volume={volume} effect={effect} composition={composition} mode={mode} thumbs={thumbs}  id={id} onAddToCart={onAddToCart}/>;
    const leftSide = <LeftSide detailImage={detailImage}/>;

    return (<div className={`product section minus-mb-185 ${reverse ? 'section--leaf' : ''}`} id={`product-${id}`}>
        <div className="section-inner">
            <div className="row">
                {
                    !reverse
                    ? (<Fragment>
                            {leftSide}
                            {rightSide}
                        </Fragment>)
                    : (<Fragment>
                            {rightSide}
                            {leftSide}
                        </Fragment>)
                }
            </div>
        </div>
    </div>);
};

const LeftSide = ({detailImage}) => {
    return (
        <div className="col-6">
            <img src={detailImage} alt=""/>
        </div>
    )
}

const RightSide = ({id, title, price, volume, thumbs, onAddToCart, effect, composition, mode}) => {
    return (
        <div className="col-6">
            <div className="product__content">
                <span className="section-policy__title detail-title section-title">{title}</span>
                <div className="product__tabs">
                    <ItemTabs effect={effect} composition={composition} mode={mode}/>
                </div>
                <ItemBottom price={price} volume={volume} thumbs={thumbs} id={id} onAddToCart={onAddToCart}/>
            </div>
        </div>
    )
}

const ItemTabs = ({id = 0, effect, composition, mode}) => {
    return (
        <Tabs id={`product-${id}`}>
            <Tab eventKey="effect" default title="Воздействие">
                <div dangerouslySetInnerHTML={{__html: effect}}/>
            </Tab>
            <Tab eventKey="composition" title="Состав">
                <div dangerouslySetInnerHTML={{__html: composition}}/>
            </Tab>
            {
                mode
                    ? (
                        <Tab eventKey="mode" default title="Способ применения">
                            <div dangerouslySetInnerHTML={{__html: mode}}/>
                        </Tab>
                      )
                    : ''
            }
        </Tabs>
    )
}

const ItemBottom = ({price, thumbs, id, volume, onAddToCart}) => {
    return (<div className="product__content-bottom">
        <span className="product__price">{price} руб</span><span className='product__volume'>{volume}</span>
        <div className="section-additionally-block-item-block-button clickAddCart add-to-cart" onClick={() => onAddToCart(id)}>
            <div className="section-additionally-block-item-block-button-text add-to-cart-with-arrow"/>
        </div>
        <ItemThumbs thumbs={thumbs}/>
        <div className="images-slider-control">
            <div className="arrows"/>
        </div>
    </div>);
};

const ItemThumbs = ({thumbs}) => {
    const settings = {
        dots: false,
        //appendArrows: container.find('.images-slider-control .arrows'),
        nextArrow: <NextArrow/>,
        prevArrow: <PrevArrow/>,
        slidesToShow: 3
    };

    return (
        <div className="product-images">
            <Slider {...settings}>
                {
                    thumbs.map((thumb) => {
                        return (
                            <div key={thumb} className="product-images__item">
                                <img src={thumb} alt=""/>
                            </div>
                        )
                    })
                }
            </Slider>
        </div>
    )
}

const NextArrow = ({ className, style, onClick }) => {
    return (
        <span
            className={`next ${className}`}
            style={{ ...style}}
            onClick={onClick}
        />
    );
};


const PrevArrow = ({ className, style, onClick }) => {
    return (
        <span
            className={`prev ${className}`}
            style={{ ...style}}
            onClick={onClick}
        />
    );
};

export default Item