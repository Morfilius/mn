import React from "react";
import Item from "./item";
import Set from "./set";

const Detail = ({product, onAddToCart}) => {
    if (product.type === 'item') {
        return <Item {...product} onAddToCart={onAddToCart} />
    } else if (product.type === 'set') {
        return <Set {...product} onAddToCart={onAddToCart}/>
    }

};

export default Detail