import React, {Fragment} from 'react';

const SuccessItem = ({title, price, count}) => {
    return (
        <Fragment>
            <div className="section-sp-block-line"></div>
            <div className="section-sp-block-text">
                <div className="section-sp-block-text-first">
                    <div>{title}</div>
                    <div>{price * count} руб.</div>
                </div>
                <div className="section-sp-block-text-end">Количество: {count} шт</div>
            </div>
        </Fragment>
    );
};

export default SuccessItem;