import React, {useEffect} from "react";
import Products from "../products/products";
import Details from "../details/details";
import Relation from "../relation/relation";
import WithAppService from "../hoc-helpers/with-app-service";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import GoogleTagService from "../../services/GoogleTagService";

const HomePage = ({products, onAddToCart, scrollTop, getData, setProduct}) => {

    const tagService = new GoogleTagService();

    useEffect(() => {
        getData().then((data) => {
            setProduct(data)
            tagService.showProducts(data);
        })
    },[getData,setProduct])





    return (
        <div className="window">
            <main className="main">
                <div className="section-policy section-outer item4">
                    <img className="section-policy-leaf-1" src="./img/icon-leaf-1.png"/>
                        <div className="section-inner">
                            <div className="section-policy__inner">
                                <div className="row">
                                    <div className="col-6">
                                        <div className="section-policy__img-wrap">
                                            <img className="section-policy__img" src="img/policy.jpg" alt=""/>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className="section-policy__content">
                                            <Tabs>
                                                <Tab eventKey="about" default title="О нас">
                                                    <div className="preview-text">
                                                        <p>
                                                            Make’n’nature - это вдохновение самой природой. Мы обратили внимание на ресурсы растительного мира и включили их в состав наших продуктов.
                                                            Для Вашей красоты  Make’n’nature собирает сырье наивысшего качества в самых разнообразных уголках планеты ,сохранивших свою первозданную чистоту и свежесть!
                                                            Ведь от этого, напрямую, зависит безопасность и эффективность нашей продукции.
                                                        </p>
                                                        <p>
                                                            Мы стараемся для Вас максимально раскрывать целебную силу растений для поддержания и сохранения естественных восстанавливающих сил организма.
                                                        </p>
                                                        <p>
                                                            Обращая внимания на мелочи, мы поняли, что женщинам важно и нужно в средствах по уходу за собой.
                                                        </p>
                                                    </div>
                                                </Tab>
                                                <Tab eventKey="result" default title="Результаты">
                                                    <div className="preview-text">
                                                        <p>
                                                            Нашей идеей было создание абсолютно новой линии продуктов, доступной каждой женщине.
                                                            Постоянное совершенствование рецептов и технологий производства позволили нам за  9 месяцев добиться идеального сочетания ингредиентов,
                                                            делающих вашу кожу мягкой и здоровой. Сейчас у всех линеек Make’n’nature имеются сертификаты ГОСТ и доказанные клинические свойства.
                                                            Мы дарим возможность прикоснуться к продуктам из натуральных компонентов абсолютно нового уровня и почувствовать их эффект.
                                                        </p>
                                                    </div>
                                                </Tab>
                                                <Tab eventKey="mission" default title="Миссия">
                                                    <div className="preview-text">
                                                        <p>
                                                            Мы хотим, чтобы образ качественных продуктов с невероятными свойствами крепко закрепился за «Make’n’nature», а история нашего бренда стала его неотъемлемой частью.
                                                            Любите и цените себя еще больше!
                                                            А с нашими продуктами, уход за душой и телом всегда будет доставлять радость!
                                                        </p>
                                                    </div>
                                                </Tab>
                                            </Tabs>
                                            <a href="#" onClick={(e) => {
                                                e.preventDefault()
                                                scrollTop(1000, -150)
                                            }} className="show-all-goods"></a>
                                            <div className="section-policy__img-bottom-wrap">
                                                <img className="section-policy__img-bottom" src="img/img-bottom.jpg"
                                                     alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <Products products={products} onAddToCart={onAddToCart} scrollTop={scrollTop}/>
                <Details products={products} onAddToCart={onAddToCart} scrollTop={scrollTop}/>
                <Relation products={products} onAddToCart={onAddToCart}/>
            </main>
        </div>
    );
};

const ItemTabs = ({id = 0, effect, composition, mode}) => {
    return (
        <Tabs id={`product-${id}`}>
            <Tab eventKey="effect" default title="Воздействие">
                <div dangerouslySetInnerHTML={{__html: effect}}/>
            </Tab>
            <Tab eventKey="composition" title="Состав">
                <div dangerouslySetInnerHTML={{__html: composition}}/>
            </Tab>
            {
                mode
                    ? (
                        <Tab eventKey="mode" default title="Способ применения">
                            <div dangerouslySetInnerHTML={{__html: mode}}/>
                        </Tab>
                    )
                    : ''
            }
        </Tabs>
    )
}

const mapMethodsToProps = (appService) => {
    return {
        getData: appService.getData
    }
};

export default WithAppService(mapMethodsToProps)(HomePage);