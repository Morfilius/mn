import React, {useEffect} from 'react';
import {Link} from "react-router-dom";
import SuccessItem from "../success-item/success-item";
import GoogleTagService from "../../services/GoogleTagService";

const SuccessPage = ({cart, orderNumber, totalCost, deliveryCost, formFields, isUsePromo}) => {

    const tagService = new GoogleTagService();

    useEffect(() => {
        window.scrollTo(0, 0);
        tagService.purchase(cart, orderNumber, totalCost, deliveryCost, isUsePromo)
    },[]);

    const sendData = () => {
        let formData = new FormData();
        formData.append("phone", formFields.phone);
        formData.append("locality", formFields.locality);
        formData.append("street", formFields.street);
        formData.append("house", formFields.house);
        formData.append("flat", formFields.flat);
        formData.append("name", formFields.firstName);
        formData.append("surname", formFields.lastName);
        formData.append("email", formFields.email);
        formData.append("order_num", orderNumber);
        formData.append("sum_car", totalCost);
        formData.append("delivery", deliveryCost);
        formData.append("pay", formFields.method);
        formData.append("comment", formFields.comment);
        formData.append("code", formFields.code);
        cart.forEach((item) => {
            formData.append("goods[]", `${item.product.title}|${item.product.price}|${item.count}`);
        })

        let response = fetch('/handler.php', {
            method: 'POST',
            body: formData
        });
        response
            .then((response) => response.text())
            .then((data) => {
                window.location.replace(data);
            })
    }


    return (
        <div className="section-sp section-outer cart-final">
            <img className="section-sp-leaf1" src="./img/icon-sp-r-1.png"/>
                <img className="section-sp-leaf2" src="./img/icon-sp-r-2.png"/>
                <Link className="section-sp-close" to="/"><img src="./img/icon-close-sp.svg"/></Link>
                    <div className="section-io">
                        <div className="section-sp-block">
                            <div className="section-sp-block-headline">Заказ № {orderNumber}</div>
                            <div className="showCek">
                                {
                                    cart.map((item) => {
                                        return (
                                            <SuccessItem
                                                key={item.id}
                                                count={item.count}
                                                price={item.product.price}
                                                title={item.product.title}
                                            />
                                        )
                                    })
                                }
                            </div>
                            <div className="section-sp-block-line"></div>
                            <div>Доставка: <span className="deliveryFinal">{deliveryCost}</span> руб.</div>
                            <div className="section-sp-block-itog">Итого: <span
                                className="showAllPrice">{totalCost}</span> руб.
                            </div>
                            <div className="basket-block-of-form-left-per" id="pay">
                                <button onClick={sendData} className="orderButton" value="Отправить заявку">Оплатить<img alt='' src="./img/arrow1.svg"/></button>
                            </div>
                        </div>
                    </div>
        </div>
    )
};

export default SuccessPage;