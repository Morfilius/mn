import React, {useEffect} from 'react';
import {Link, Redirect} from "react-router-dom";
import Cart from "../cart/cart";
import OrderForm from "../order-form/order-form";
import GoogleTagService from "../../services/GoogleTagService";

const CartPage = ({cart, increaseCountCartItem, decreaseCountCartItem, assignOrderNumber,
                      totalCost, assignTotalCost, deleteItemCart, assignDeliveryCost, assignFormFields, orderNumber, togglePromo}) => {

    const tagService = new GoogleTagService();

    useEffect(() => {
        window.scrollTo(0, 0);
        tagService.checkout(cart)
    },[]);

    if(orderNumber > 0) {
        return <Redirect to='/success' />;
    }

    return (
        <div className='window'>
            <div className="basket">
                <img className="basket-leaf1" src="./img/icon-ba-leaf-1.png"/>
                <img className="basket-leaf2" src="./img/icon-ba-leaf-2.png"/>
                <Link className="section-sp-close" to="/"><img src="./img/icon-close-sp.svg"/></Link>
                <div className="basket-block">

                    <Cart
                        cart={cart}
                        increaseCountCartItem={increaseCountCartItem}
                        decreaseCountCartItem={decreaseCountCartItem}
                        assignTotalCost={assignTotalCost}
                        deleteItemCart={deleteItemCart}
                    />
                    <div className="basket-block-of">
                        <div className="basket-block-of-title">Оформление заказа</div>
                        <div className="basket-block-of-form">
                            <div className="basket-block-of-form-left">

                                <OrderForm assignDeliveryCost={assignDeliveryCost} totalCost={totalCost}
                                           assignFormFields={assignFormFields} assignOrderNumber={assignOrderNumber} togglePromo={togglePromo}/>

                            </div>
                            <div className="basket-block-of-form-right">
                                Возникли вопросы по доставке или оплате, свяжитесь с нами:
                                <br/><br/>
                                +88006002758 (пн-пт с 10:00 - 18:00 )
                                <br/>
                                +84991101120 (пн-пт с 10:00 - 18:00 )
                                <br/><br/>
                                info@makeandnature.ru (24/7)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default CartPage;