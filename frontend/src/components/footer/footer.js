import React from "react";


const Footer = ({products, scrollTop, setChangeModalShow, setDeliveryModalShow, setPayModalShow}) => {
    return (<div className="section-footer section-outer contact">
        <div className="section-inner">
            <div className="row">
                <div className="col-3">
                    <span className="section-footer-block-text-headline">Продукты</span>
                    <ul className="footer-menu">
                        {
                            products.filter((item) => item.type === 'item').map((item) => {
                                return (
                                    <li key={item.id} className="footer-menu__item">
                                        <a href="#" onClick={(e) => {
                                            e.preventDefault();
                                            scrollTop(item.id, item.offset)
                                        }} className="footer-menu__link">{item.short}</a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                <div className="col-3">
                    <span className="section-footer-block-text-headline">Информация</span>
                    <ul className="footer-menu">
                        <li className="footer-menu__item">
                            <a href="#" onClick={(e) => {
                                e.preventDefault()
                                setChangeModalShow(true)
                            }} className="footer-menu__link">Обмен
                                и возврат</a>
                        </li>
                        <li className="footer-menu__item">
                            <a href="#" onClick={(e) => {
                                e.preventDefault()
                                setDeliveryModalShow(true)
                            }} className="footer-menu__link">Доставка</a>
                        </li>
                        <li className="footer-menu__item">
                            <a href="/dec.pdf" download className="footer-menu__link">Декларация о
                                соответствии</a>
                        </li>
                        <li className="footer-menu__item">
                            <a href="#"  onClick={(e) => {
                                e.preventDefault()
                                setPayModalShow(true)
                            }} className="footer-menu__link">Способы оплаты</a>
                        </li>
                        <li className="footer-menu__item">
                            <img src="img/payment.png" alt=""/>
                        </li>
                    </ul>
                </div>
                <div className="col-3">
                    <span className="section-footer-block-text-headline">Информация</span>
                    <ul className="footer-menu">
                        <li className="footer-menu__item">
                            <span className="footer-menu__link">Площадь Журавлёва 2с2</span>
                        </li>
                        <li className="footer-menu__item">
                            <a href="mailto:Info@makeandnature.ru "
                               className="footer-menu__link">Info@makeandnature.ru </a>
                        </li>
                        <li className="footer-menu__item">
                            <span className="footer-menu__link">пн-пт с 10:00 - 18:00 </span>
                        </li>
                        <li className="footer-menu__item">
                            <a href="tel:+88006002758" className="footer-menu__link">+88006002758</a>
                        </li>
                        <li className="footer-menu__item">
                            <a href="tel:+84991101120" className="footer-menu__link">+84991101120</a>
                        </li>
                    </ul>
                </div>
                <div className="col-3">
                    <img src="./img/icon-logo-footer.svg" alt=""/>
                </div>
            </div>
        </div>
    </div>)
};

export default Footer