import React from "react";
import Card from "../card/card";

const Products = ({products, onAddToCart, scrollTop}) => {
    const singleCard = products.filter((product) => product.id === 7 && product.type === 'item');
    return (
        <div id='product-1000'>
            <div className="section-products section-outer" id="products">
                <img className="section-products-leaf-2" src="./img/icon-leaf-2.png"/>
                <img className="section-products-leaf-3" src="./img/icon-leaf-3.png"/>
                <img className="section-products-leaf-4" src="./img/icon-leaf-4.png"/>

                <div className="section-inner">
                    <div className="section-products__inner">
                                        <span
                                            className="section-products-block-left-headline">Товары Make’n’nature</span>
                        <div className="section-line"></div>
                        <div className="section-products__content">
                            <div className="row">
                                {
                                    products.filter((product) => product.id !== 7 && product.type === 'item').map((product) => {
                                        return (
                                            <div key={product.id} className="col-4">
                                                <Card {...product} onAddToCart={onAddToCart} scrollTop={scrollTop}/>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="section-brushes section-outer item1 pb100">
                <div className="section-inner">
                    <div className="row">
                        <div className="col-4">

                            <Card {...singleCard[0]} onAddToCart={onAddToCart} scrollTop={scrollTop}/>
                        </div>
                        <div className="col-8">
                            <div className="card">
                                <div className="card__img-wrap">
                                        <span style={{ backgroundImage:  `url(./img/img-title.svg)` }}
                                              className="card__img-title"></span>
                                    <img className="section-additionally-block-item-block-img card__img" alt=""
                                         src="./img/products-preview.jpg"/>
                                </div>
                                <span className="card__subtitle">
                                        Make’n’nature
                                    </span><br/>
                                <span
                                    className="section-additionally-block-item-block-hedline card__title">Товары <br/>Make’n’nature </span><br/>
                                <a href="#" onClick={(e) => {
                                    e.preventDefault()
                                    scrollTop(1000, -150)
                                }} className="show-more"></a>
                                <p className="card__text">
                                    Мы хотим, чтобы образ качественных продуктов с невероятными свойствами крепко закрепился за Make’n’nature, а история нашего бренда стала его неотъемлемой частью
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
};

export default Products