import React from "react";
import Detail from "../detail/detail";

const Details = ({products, onAddToCart}) => {
    return (
        <div className='details'>
            {
                products.map((product) => {
                    return <Detail key={product.id} product={product} onAddToCart={onAddToCart}/>
                })
            }
        </div>
    );
};

export default Details