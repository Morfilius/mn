export default class GoogleTagService {

    dataLayer = window.dataLayer

    showProducts = (products) => {


        const impressions = products.filter( product => product.type === 'item' ).map( (product, idx) => {
            return {
                'name': product.title,	    // Название товара
                'id': String(product.id),			// Идентификатор товара
                'price': String(product.price),		// Стоимость товара (актуальная)
                'brand': 'Make’n’Nature',	// Название бренда (если у товара/услуги нет бренда, то указать '' или 'Make’n’Nature')
                'category': 'без категории',	// Название категории (желательно - наиболее узкой
                'list': 'Каталог_товаров',	// Так как больше других страниц и разделов нет, то  это значение оставить во всех кодах показа товаров
                'position': +idx + 1		// Позиция товара, считая от левого верхнего угла.           СТРОГО со значения 1.
            }
        })
        window.onload = () => {
            setTimeout(() => {
                this.dataLayer.push({
                    'event': 'showProducts',
                    'ecommerce': {
                        'currencyCode': 'RUB',// Валюта товара
                        'impressions': impressions
                    }
                });
            }, 20)
        };
    };

    addToCart = (product) => {
        this.dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'RUB',
                'add': {
                    'products': [
                        {
                            'name': product.title,	     // Название товара
                            'id': String(product.id),			 // Идентификатор товара
                            'price': String(product.price),		 // Стоимость товара
                            'brand': 'Make’n’Nature',	 // Название бренда
                            'category': 'без категории', // Название категории
                            'quantity': 1				 // Количество товаров, добавленных в корзину (без 'кавычек')
                        }
                    ]
                }
            }
        });
    }

    removeFromCart = (product) => {
        this.dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': 'RUB',
                'remove': {
                    'products': [
                        {
                            'name': product.title,	     // Название товара
                            'id': String(product.id),			 // Идентификатор товара
                            'price': String(product.price),		 // Стоимость товара
                            'brand': 'Make’n’Nature',	 // Название бренда
                            'category': 'без категории', // Название категории
                            'quantity': 1				 // Количество товаров, добавленных в корзину (без 'кавычек')
                        }
                    ]
                }
            }
        });
    }

    checkout = (products) => {
        if (products.length) {
            const cart_items = products.map( (product, idx) => {
                return {
                    'name': product.product.title,	    // Название товара
                    'id': String(product.product.id),			// Идентификатор товара
                    'price': String(product.product.price),		// Стоимость товара (актуальная)
                    'brand': 'Make’n’Nature',	        // Название бренда (если у товара/услуги нет бренда, то указать '' или 'Make’n’Nature')
                    'category': 'без категории',	    // Название категории (желательно - наиболее узкой
                    'list': 'Каталог_товаров',	        // Так как больше других страниц и разделов нет, то  это значение оставить во всех кодах показа товаров
                    'quantity': product.count
                }
            })

            this.dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                    'checkout': {
                        'actionField': {'step': 1},
                        'products': cart_items
                    }
                }
            });
        }
    }

    purchase = (products, orderNumber, totalCost, deliveryCost, isUsePromo) => {
        const cart_items = products.map( (product, idx) => {
            return {
                'name': product.product.title,	    // Название товара
                'id': String(product.product.id),			// Идентификатор товара
                'price': String(product.product.price),		// Стоимость товара (актуальная)
                'brand': 'Make’n’Nature',	        // Название бренда (если у товара/услуги нет бренда, то указать '' или 'Make’n’Nature')
                'category': 'без категории',	    // Название категории (желательно - наиболее узкой
                'list': 'Каталог_товаров',	        // Так как больше других страниц и разделов нет, то  это значение оставить во всех кодах показа товаров
                'quantity': product.count
            }
        })

        this.dataLayer.push({
            'event': 'purchase',
            'ecommerce': {
                'purchase': {
                    'actionField': {
                        'id': String(orderNumber),				 // Номер заказа
                        'affiliation': 'Онлайн-магазин',
                        'shipping': String(deliveryCost),		 // Стоимость доставки
                        'revenue': String(totalCost),			 // Общая сумма заказа (общая, с суммой доставки или без нее - выбрать один вариант, “Доход” будет поступать в системы аналитики из этого параметра)
                        'Tax':'',					     // Налог. Возможно установление пустого значения.
                        'coupon': isUsePromo ? 'GREEN' : ''			 // Купон, промокод. Возможно установление пустого значения.
                    },
                    'products': cart_items
                }
            }
        });

    }
}