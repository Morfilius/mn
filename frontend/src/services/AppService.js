export default class AppService {
    _apiBase = '';

    getData = async () => {
        return await this._getResource(`/data/data.php`);
    }

    _getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };
}