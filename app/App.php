<?php


namespace app;


use YandexCheckout\Client;
use YandexCheckout\Request\Payments\CreatePaymentResponse;

class App
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Client
     */
    private $client;

    public function __construct(Request $request, Client $client)
    {
        $this->request = $request;
        $this->client = $client;
    }

    public function getMeta()
    {
        return [
            'order_num' => $this->request->order_num,
            'phone' => $this->request->phone,
            'locality' => $this->request->locality,
            'street' => $this->request->street,
            'house' => $this->request->house,
            'flat' => $this->request->flat,
            'name' => $this->request->name,
            'surname' => $this->request->surname,
            'email' => $this->request->email,
            'sum_car' => $this->request->sum_car,
            'comment' => $this->request->comment,
            'delivery' => $this->request->getDelivery(),
            'goods' => array_reduce($this->request->getGoods(), function ($result, $item) {
                $result.=$item[0] . ' x ' . $item[2] . '; ';
                return $result;
            }, ''),
        ];
    }

    public function createPayment($return_url) : CreatePaymentResponse
    {
        $body = array(
            "amount" => array(
                "value" => $this->request->sum_car,
                "currency" => "RUB"
            ),
            "confirmation" => array(
                "type" => "redirect",
                "return_url" => $return_url
            ),
            'metadata' => $this->getMeta(),
            "receipt" => array(
                "customer" => array(
                    "full_name" => $this->request->name . ' ' . $this->request->surname,
                    "email" => $this->request->email,
                ),
                "items" => array_map(function ($good) {
                    return array(
                        "description" => $good[0],
                        "quantity" => $good[2],
                        "amount" => array(
                            "value" => $good[1],
                            "currency" => "RUB"
                        ),
                        "vat_code" => "2",
                        "payment_mode" => "full_prepayment",
                        "payment_subject" => "commodity"
                    );
                }, $this->request->getGoods())
            )
        );
        if ($this->request->getDelivery() > 0) {
            $body['receipt']['items'][] = array(
                "description" => 'Доставка',
                "quantity" => 1,
                "amount" => array(
                    "value" => $this->request->getDelivery(),
                    "currency" => "RUB"
                ),
                "vat_code" => "2",
                "payment_mode" => "full_prepayment",
                "payment_subject" => "commodity"
            );
        }
        return $this->client->createPayment(
            $body,
            uniqid('', true)
        );
    }
}