<?php


namespace app;


class Request
{
    public $phone;
    public $locality;
    public $street;
    public $house;
    public $flat;
    public $name;
    public $surname;
    public $email;
    public $comment;
    public $order_num;
    public $sum_car;
    private $goods;
    private $delivery;
    private $pay;

    public function __construct(array $post)
    {
        $this->phone = strip_tags($post['phone']);
        $this->locality = strip_tags($post['locality']);
        $this->street = strip_tags($post['street']);
        $this->house = strip_tags($post['house']);
        $this->flat = strip_tags($post['flat']);
        $this->name = strip_tags($post['name']);
        $this->surname = strip_tags($post['surname']);
        $this->email = strip_tags($post['email']);
        $this->comment = strip_tags($post['comment']);
        $this->order_num = (int)$post['order_num'];
        $this->sum_car = (int)$post['sum_car'];
        $this->delivery = (int)$post['delivery'];
        $this->pay = (int)$post['pay'];
        $this->goods = $post['goods'];
    }

    public function getGoods() {
        if ($this->goods && is_array($this->goods)) {
             return array_map(function ($good) {
                 return explode('|', $good);
             }, $this->goods);
        }
        return [];
    }

    public function getDelivery()
    {
        return $this->delivery;
    }

    public function getPay()
    {
        if ($this->pay == 2) {
            return 'предоплата (картой онлайн)';
        } elseif ($this->pay == 1) {
            return 'оплата при получении';
        }
        return '';
    }
}