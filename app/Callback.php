<?php


namespace app;


class Callback
{
    private $bodyRequest;
    private $bodyObject;
    private $metadata;

    public function __construct()
    {
        if ($this->isJSONRequest()) {
            $this->bodyRequest = json_decode(file_get_contents('php://input'));
            if ($this->isBodyNotEmpty()) {
                $this->bodyObject = $this->bodyRequest->object;
                $this->metadata = $this->bodyObject->metadata;
            }
        }
    }

    public function isJSONRequest()
    {
        return $_SERVER['HTTP_CONTENT_TYPE'] == 'application/json' || $_SERVER['HTTP_ACCEPT'] == 'application/json';
    }

    public function isBodyNotEmpty()
    {
        return isset($this->bodyRequest->object);
    }

    public function isSuccessPayment()
    {
        return $this->isBodyNotEmpty() && $this->bodyObject->status == 'succeeded';
    }

    public function sendMail($to)
    {
        if (!$this->isBodyNotEmpty()) return;
        mail(
            $to,
            "New order " . $this->metadata->order_num, "У вас новый заказ \n
            Телефон: " . $this->metadata->phone . " \n
            Населённый пункт: " . $this->metadata->locality . " \n
            Адрес доставки: улица " . $this->metadata->street . ", дом " . $this->metadata->house . ", квартира " . $this->metadata->flat . " \n" .

            //Дата доставки: " . $inputDate . " \n
            "Имя: " . $this->metadata->name . " \n
            Фамилия: " . $this->metadata->surname . " \n
            Email: " . $this->metadata->email . " \n
            Комментарий: " . $this->metadata->comment . " \n\n
            Товары: \n"
            . $this->metadata->goods . " \n\n
            Доставка: " . $this->metadata->delivery . " \n
            Оплата: Оплата на сайте \n
            
            Итого к оплате: " . $this->metadata->sum_car . " \n \n "

        );
    }
}